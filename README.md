# DITA Merge REST Java Command-Line Interface

This sample command-line driver can be used to experiment with DITA Merge's REST API. Please note that the REST server must be running for this code to operate.
This client connects to http://localhost:8080 by default, but you can use the property "host" to customize the host, e.g. -Dhost=http://localhost:1234 to point to the REST service running on a different port to the default of 8080.

This command-line driver was written as proof of concept to demonstrate use of the DITA Merge REST API, and is not intended as a primary way of interacting with it. The source code for this command-line driver is available as a Maven project. Please customize as needed. Currently, it uses asynchronous calls with multipart/form-data. This sample processes responses as Strings and uses XPath to get information from the responses.


## List the Available Commands
####java -jar deltaxml-dita-merge-rest-client-3.0.0.jar
		
	Usage:
 	java -jar deltaxml-dita-merge-rest-client-x.y.z.jar
 	java -Dhost=http://localhost:1234 -jar deltaxml-dita-merge-rest-client-x.y.z.jar
 	java -jar deltaxml-dita-merge-rest-client-x.y.z.jar describe <mergeType> to see the description of the available parameters for the specified merge type.
 	java -jar deltaxml-dita-merge-rest-client-x.y.z.jar merge <mergeType> <ancestorName/version1Name> <ancestorFile/version1File> (<versionName> <versionFile>)+ <resultFile> <params>

 	Available Merge Types:
 	====================================================
 	Merge Type        | Short Description
 	====================================================
 	concurrent        | N-Way concurrent merge
 	concurrent3       | Three way concurrent merge
 	sequential        | N-Way sequential merge
 	====================================================
	
## Describe the Merge Type
####java -jar deltaxml-dita-merge-rest-client-3.0.0.jar describe concurrent

 	A concurrent merge requires an ancestor file and at least two revision files. Each file needs a name.

 	java -jar deltaxml-dita-merge-rest-client-x.y.z.jar merge concurrent <ancestorName> <ancestorFile> (<versionName> <versionFile>)+ <resultFile> <params>

 	The following are the allowed params and their default values:
	=======================================================================
	Param                               | Default Values
	=======================================================================
	ResultType                          | DELTAV2
	-----------------------------------------------------------------------
 	WordByWord                          | true
	-----------------------------------------------------------------------
	ElementSplitting                    | true
	-----------------------------------------------------------------------
	CalsTableProcessing                 | true
	-----------------------------------------------------------------------
 	HtmlTableProcessing                 | true
	-----------------------------------------------------------------------
 	Indent                              | false
	-----------------------------------------------------------------------
 	DoctypePreservationMode             | PRESERVE_WHEN_UNCHANGED
	-----------------------------------------------------------------------
 	EntityReferencePreservationMode     | PRESERVE_REFERENCES
	-----------------------------------------------------------------------
 	CalsValidationLevel                 | RELAXED
	-----------------------------------------------------------------------
 	InvalidCalsTableBehaviour           | PROPAGATE_UP
	-----------------------------------------------------------------------
 	WarningReportMode                   | PROCESSING_INSTRUCTIONS
	=======================================================================
	
## Run a Merge
####java -jar deltaxml-dita-merge-rest-client-3.0.0.jar merge concurrent base samples/html-data/four-edits.html anna samples/html-data/four-edits-anna.html ben samples/html-data/four-edits-ben.html result.xml Indent=true

	Progress:  
		STARTED
			Processing Ancestor 'base'
			Processing Version 'anna'
			Processing Version 'ben'
		EXTRACTING
			Extracting Merge Result
		SUCCESS

 	Result written to result.xml